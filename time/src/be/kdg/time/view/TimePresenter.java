package be.kdg.time.view;

import be.kdg.time.model.TimeModel;

import java.time.LocalTime;

public class TimePresenter {
	private TimeModel model;
	private TimeView view;

	public TimePresenter(TimeModel model, TimeView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getSlider().setOnMouseDragged(e -> {
			double sliderValue = view.getSlider().getValue();
			int hours = (int) sliderValue;
			double fraction = sliderValue - hours;
			int minutes = (int) Math.round(60 * fraction);
			model.setCurrentTime(LocalTime.of(hours, minutes));
			updateView();
		});
	}

	private void updateView() {
		view.applyDaylightSun(model.getDaylightPercentage(), model.getSunHeight(), model.getSunPositionX());
		final LocalTime currentTime = model.getCurrentTime();
		final int hour = currentTime.getHour();
		final int minute = currentTime.getMinute();
		double fraction = (double) minute / 60;
		double newSliderValue = hour + fraction;
		view.getSlider().setValue(newSliderValue);
	}
}
