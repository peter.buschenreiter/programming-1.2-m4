package be.kdg.cityhall.view;

public class CityHallPresenter {
	private final CityHallView view;

	public CityHallPresenter(CityHallView view) {
		this.view = view;

		this.addEventHandlers();
	}

	private void addEventHandlers() {
		view.getNormal().setOnAction(actionEvent -> view.resetEffect());
		view.getBlackAndWhite().setOnAction(actionEvent -> view.applyBlackAndWhiteEffect());
		view.getSepia().setOnAction(actionEvent -> view.applySepiaEffect());
	}
}
