package be.kdg.cityhall.view;

import javafx.geometry.Insets;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class CityHallView extends VBox {
	private static final Image IMAGE = new Image("/cityHall.jpg");

	private ImageView imageView;
	private RadioButton normal;
	private RadioButton blackAndWhite;
	private RadioButton sepia;

	public CityHallView() {
		initializeNodes();
		layoutNodes();
	}

	private void initializeNodes() {
		imageView = new ImageView(new Image("cityHall.jpg"));
		normal = new RadioButton("Normal");
		blackAndWhite = new RadioButton("Black & White");
		sepia = new RadioButton("Sepia");
		ToggleGroup toggleGroup = new ToggleGroup();
		toggleGroup.getToggles().addAll(normal, blackAndWhite, sepia);
		normal.setSelected(true);
	}

	private void layoutNodes() {
        getChildren().addAll( imageView, normal, blackAndWhite, sepia);
		getChildren().forEach(node -> setMargin(node, new Insets(15)));
	}

	RadioButton getNormal() {
		return normal;
	}

	RadioButton getBlackAndWhite() {
		return blackAndWhite;
	}

	RadioButton getSepia() {
		return sepia;
	}

	void resetEffect() {
		this.imageView.setEffect(null);
	}

	void applyBlackAndWhiteEffect() {
		ColorAdjust blackAndWhite = new ColorAdjust();
		blackAndWhite.setSaturation(-1.0);
		this.imageView.setEffect(blackAndWhite);
	}

	void applySepiaEffect() {
		SepiaTone sepiaTone = new SepiaTone();
		sepiaTone.setLevel(0.8);
		this.imageView.setEffect(sepiaTone);
	}
}
