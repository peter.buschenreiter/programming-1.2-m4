package be.kdg.cityhall;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import be.kdg.cityhall.view.CityHallPresenter;
import be.kdg.cityhall.view.CityHallView;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        final CityHallView view = new CityHallView();
        final CityHallPresenter cityHallPresenter = new CityHallPresenter(view);
        primaryStage.setTitle("City Hall");
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
