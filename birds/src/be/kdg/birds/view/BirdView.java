package be.kdg.birds.view;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;


public class BirdView extends BorderPane {
	private MenuItem menuItem;
	private CheckBox checkBox;
	private final Image bird = new Image("/angryBird.png");

	public BirdView() {
		this.initializeNodes();
		this.layoutNodes();
	}

	private void initializeNodes() {
		menuItem = new MenuItem(null, new ImageView(bird));

		checkBox = new CheckBox();
		checkBox.setGraphic(new ImageView(bird));
	}

	private void layoutNodes() {
		Menu menu = new Menu(null, new ImageView(bird), menuItem);
		MenuBar menuBar = new MenuBar(menu);
		setCenter(checkBox);
		setTop(menuBar);
	}

	public Image getBird() {
		return bird;
	}
}
