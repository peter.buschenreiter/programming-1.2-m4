package be.kdg.birds;

import be.kdg.birds.view.BirdView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		BirdView birdView = new BirdView();
		Scene scene = new Scene(birdView);
		stage.setScene(scene);
		stage.setTitle("Birds");
		stage.getIcons().add(birdView.getBird());
		stage.setWidth(680);
		stage.setHeight(480);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
